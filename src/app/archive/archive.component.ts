import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.css']
})
export class ArchiveComponent implements OnInit {

  year: number;
  month: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    let paramMap = this.route.snapshot.paramMap;
    this.year = +paramMap.get('year');
    this.month = +paramMap.get('month');
  }

  handleViewAll(){
    console.log("here")
    this.router.navigate(['']);
  }
}
